package com.epam.learn.l1;

public class DefaultValue {

    ///////ЦЕЛЫЕ///////
    byte dByte;         // 0 ( в byte)
    short dShort;       // 0
    int dInt;           // 0
    long dLong;         // 0

    ///////ДРОБНЫЕ///////
    float dFloat;       // 0.0
    double dDouble;     // 0.0

    ///////ЛОГИЧЕСКИЕ///////
    boolean dBoolean;   //false

    ///////СИМВОЛЬНЫЕ///////
    char dChar;         // '\u0000' (null)

    public static void main(String[] args) {
        System.out.println(new DefaultValue ().dInt);
    }
}
