package com.epam.learn.l1;

public class DefaultPrimitive {
    public static void main(String[] args) {
        System.out.println(getSum((byte) 127, (byte) 1));
    }

    private static byte getSum (byte v1, byte v2){
        return (byte) (v1 + v2);
    }

    private static int getIntSum (int v1, int v2){
        return v1 + v2;
    }
}
